using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;

// REV: pls delete all comments after you fixed the issues

namespace CollectionsTask
{ 
    class ReadLines : IEnumerable<string>
    {
        string _path;

        public ReadLines(string path)
        {
            if (path == null || !File.Exists(path))
                throw new FileNotFoundException();    // <---- this is not correct. If the path is null or empty then its more like ArgumentException
                // also beware, the file can exist and still not be reachable

            _path = path;
        }

        public IEnumerator<string> GetEnumerator() => new Read(_path);
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


        // this class claims to be an enumerable, though due to it's implementation there will be some problems if we instantiate several enumerators at the same time. You have to fix this
        private class Read : IEnumerator<string>
        {
            static FileStream _fileStream;
            static bool isEndOfFile;

            static int pointer;
            static int numOfReadCharacters;    // <---- this is misleading, you are not reading characters
            static byte[] buffer;
            static int step;    //  <---- this var stores the size of the buffer, why name it 'step' ?? usually its buffSize, buffSz, nbytes, BUFFSZ, BUFFLEN etc. 

            public Read(string path)
            {                //  ^~~~~~~~~~~~ you do test this var in the parent class. Still, I'll add some assertions here to be on the safe side  
                             //  System.Diagnostics.Debug.Assert
                _fileStream = new FileStream(path, FileMode.Open, FileAccess.Read); //   <--- hope for a happy case :)
                step = 512;  // you should not hardcode data like that. Its better to declare a const value instead
                buffer = new byte[step];
                pointer = 0;  // no harm, but it does nothing and can bee seen as noise for someone
                numOfReadCharacters = 0; // same as above
            }

            public string Current { get; private set; }
            object IEnumerator.Current => Current;

            public bool MoveNext()
            {

                //    ^~~~~~~~ here you have an epty line after the opening bracet. Why?
                if (isEndOfFile)
                {
                    Dispose();
                    return false;
                }

                Current = ReadOneLine(); 

                //    ^~~~~~~ this empty line can be deleted as well
                return true;
            }

            public void Reset() 
            {
                throw new NotImplementedException();
            } 

            public void Dispose() 
            {
                _fileStream.Close(); // no harm, but more expected would be "_fileStream.Dispose();"
            } 


            private static string ReadOneLine()
            {
                //      v~~~~~~~~~~~v~~~~~~~~~~~ the suffix 'point' is misleading. Is this a position in the stream, or something?
                int startPoint, endPoint;
                //       ^~~~~~~ local vars are uninitialized. It's better to have a habbit to always init them with something
                //       also its not a good idea to declare your local vars at the start of the function
                //       better to declare them as close to their usage as possible

                var thisLine = new StringBuilder();

                if( numOfReadCharacters == 0 )  // this will trigger at the first run. Though may be harmless, its better to design something that does not execute in vain
                {
                    Array.Clear(buffer, 0, step);   // is this really important - to waste the time and clear the buffer? Don't we ever read only from the part of the buffer that was populated by the 'Read()' method?
                    numOfReadCharacters = _fileStream.Read(buffer, 0, step);
                    pointer = 0;   // this is a kind of stream position?
                }

                if(numOfReadCharacters < step && pointer >= numOfReadCharacters)   // complex if conditions are hard to grasp in one go
                // you can create bool local vars - their name will auto-document their usage. Something like:
                // bool hasMoreBytes = numOfReadCharacters == step;
                // isEndOfFile = !hasMoreBytes && (pointer >= numOfReadCharacters);  // god knows what this last bool is for..
                {
                    isEndOfFile = true;
                }

                startPoint = pointer;

                while (buffer[pointer++] > 0xD)  // not a good condition to detect a end of line. There are chars like 0x0, 0x1, 0x2 and so on, and they are not considered EOL
                // also, for other developer its hard to understand that this condition does. Its totally obscure. It can be easily made transparent:
                // private static IsEOL(byte b) {//your logic here//};     // <---- declare a static method with an explicit name and purpose
                // while ( IsEOL(buffer[pointer++]) ) {};                  // <---- also, a for loop will fit more nicely here
                {
                    if (pointer >= numOfReadCharacters )
                    {
                        endPoint = pointer;
                        thisLine.Append(Encoding.UTF8.GetString(buffer, startPoint, endPoint - startPoint));
                        startPoint = 0;

                        Array.Clear(buffer, 0, step);
                        numOfReadCharacters = _fileStream.Read(buffer, 0, step);
                        pointer = 0;
                    }
                }

                endPoint = pointer-1;
                pointer++;

                thisLine.Append(Encoding.Default.GetString(buffer, startPoint, endPoint - startPoint));

                return thisLine.ToString();

                // IN THE END:
                // the logic of this fucntion is somehow obscure and not clear
                // may be you can create a more transparent logic. For ex you can create as many methods as you want, and structure your big function better
            }

        }
    }
}
